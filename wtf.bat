@echo off
set VERSION=0.12
set NOTFOUND=
set DL_URL=http://www.mirbsd.org/acronyms.gz

if "%1" == "" goto HELP

:OPTS
  if "%1" == "-h"       goto HELP
  if "%1" == "/h"       goto HELP
  if "%1" == "--help"   goto HELP
  if "%1" == "/help"    goto HELP
  if "%1" == "/?"       goto HELP
  if "%1" == "-V"       goto VERSION
  if "%1" == "--version" goto VERSION
  if "%1" == "/V"       goto VERSION
  if "%1" == "/version" goto VERSION
  if "%1" == "-u"       goto UPDATE
  if "%1" == "--update" goto UPDATE
  if "%1" == "/u"       goto UPDATE
  if "%1" == "/update"  goto UPDATE

  set /U ABBR=%1
  shift
if not "%1" == "" goto OPTS
goto SEARCH

:UPDATE
  rem   Aktuelle Version im Internet:
  rem wget http://www.mirbsd.org/wtf.htm
  rem grep "CVS rev" wtf.htm | sed -e "s/.*<tt>//" -e "s/ .*//"  
  rem   lokale Version:
  rem head acronyms | grep $MirOS | sed -e "s/.*,v //" -e "s/ .*//"  
  wget %DL_URL%
  gzip -df acronyms.gz
  if exist wtf.htm del wtf.htm
goto EXIT


:HELP
  echo.
  echo WTF [/h^|/help^|/?] [/V^|/version] [/u^|/update] [acronym]
  echo.
  echo uses the file acronyms from http://www.mirbsd.org/wtf.htm to show the meaning(s) of an abbreviation
  echo.
  echo [acronym] is a positional parameter, it must be the last word in line
  echo.
  echo /h ^| /help ^| /? shows help and exits
  echo /u ^| /update    gets the new acronym file from mirbsd.org
  echo /V ^| /version   shows the versions of the batch file and the database and exits
  echo.
  echo instead of / in front of a parameter you can use - for short options and -- for long options
  echo You may use and modify WTF-DOS under the terms of the AGPL3
GOTO EXIT

:VERSION
  head acronyms | grep " @(#)$MirOS" | sed -e "s/.*,v //" -e "s/ [012][0-9]:.*//" > wtf.tmp
  set /P DBVERSION= < wtf.tmp
  echo WTF for FreeDOS version %VERSION%
  if not exist acronyms echo file acronyms not found, please use wtf /u for downloading
  if not exist acronyms goto EXIT
  echo acronyms database version %DBVERSION%
goto EXIT

:SEARCH
  if not exist acronyms echo file acronyms not found, please use wtf /u for downloading
  if not exist acronyms goto EXIT

  echo Searching for: %ABBR%
  grep -w "^%ABBR%" ACRONYMS > wtf.tmp
  if errorlevel 1 set NOTFOUND=Y
  if "%NOTFOUND%" == "Y" echo Nothing found, try another abbreviation
  if "%NOTFOUND%" == "" foxtype wtf.tmp

:EXIT
if exist wtf.tmp del wtf.tmp
